import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.GnuParser;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.cli.HelpFormatter;

public class zupki {

	/**
	 * @param args
	 */

	public static void main(String[] args) throws ParseException {

		int podzielnik = 100;
		int max = 10000;
		// Create GNU like options
		Options gnuOptions = new Options();
		gnuOptions.addOption("p", true, "Podzielnik, domyślnie 100").addOption("h", "help", false, "Krótka pomoc")
				.addOption("m", "max", true, "Maksymalna suma, domyslnie 10000");

		CommandLineParser gnuParser = new GnuParser();
		CommandLine cmd = gnuParser.parse(gnuOptions, args);

		if (cmd.hasOption("h")) {

			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp("CLITester", gnuOptions);
			System.exit(0);

		}

		if (cmd.hasOption("m")) {
			max = Integer.parseInt(cmd.getOptionValue("m"));
		}

		if (cmd.hasOption("p")) {
			podzielnik = Integer.parseInt(cmd.getOptionValue("p"));
		}

		int n, n_max, max_k, start;
		Zestaw liczby = new Zestaw(max, podzielnik);
		n = liczby.wczytaj_dane();
		max_k = liczby.max_k(n);
		System.out.println(" ilość wczytanych " + n);
		System.out.println(" max_k = " + max_k );

		start = 1;

		for (int k = start; k <= max_k; k++) {
			System.out.println("\nBadanie zestawów " + k + " elementowych");
			n_max = liczby.wywal_zaduze(n, k);

			liczby.kombinacje(n_max, k);
		}
		System.out.println("------------ Koniec obliczeń ---------------");
	}

}
